package com.it_system.moviedirectory.Util;

import android.app.Activity;
import android.content.SharedPreferences;

public class Prefs {
    SharedPreferences sharedPreferences;

    public Prefs(Activity activity){
        this.sharedPreferences = activity.getPreferences(activity.MODE_PRIVATE);
    }

    //Save searched therm to Shared Preferences
    public void setSearch(String search){
        sharedPreferences.edit().putString("search", search).commit();
    }

    public String getSearch() {
        return sharedPreferences.getString("search", "Batman"); //TODO: Change this default to something more useful
    }
}
